﻿
Partial Class UnitTest
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Me.Literal1.Text = DoSomething()

    End Sub

    Private Function DoSomething() As String
        For Each tableName In {"Customers", "Plans"}
            CopyTable(tableName)
        Next
        Return GetHtm(GetData(GetSql()))

    End Function

    Private Function GetSql() As String
        Return _
            <sql>
            SELECT * FROM [Plans] ORDER BY 1   
            </sql>

    End Function

    Private Function GetHtm(dt As Data.DataTable) As String
        Using adHoc As New Framework_Code.AdhocData(dt)
            Return adHoc.ToHtmlTable

        End Using

    End Function

    Private Function GetData(sql As String) As Data.DataTable
        Using rs As New Framework_Code.DataAccess("ES")
            Return rs.ExecuteQuery(sql)

        End Using

    End Function

    Private Sub CopyTable(tableName As String)
        Using rs As New Framework_Code.DataAccess("Source")
            rs.ExecuteReaderMethod(String.Format("SELECT * FROM [{0}]", tableName), tableName, AddressOf SyncTable)
        End Using

    End Sub

    ' Called from CopyTable()
    Private Sub SyncTable(tableName As String, oReader As System.Data.Common.DbDataReader)
        Using rs As New Framework_Code.TxnWriteSqlServer("Target")
            Try
                rs.ExecuteNonQuery(String.Format("TRUNCATE TABLE [{0}]", tableName))
                rs.ExecuteBulkCopy(tableName, oReader, New System.Collections.Generic.Dictionary(Of String, String))
                rs.Commit()
            Catch
                rs.Rollback()
                Throw
            End Try

        End Using

    End Sub

End Class
