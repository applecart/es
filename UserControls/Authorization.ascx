<%@ Control Language="C#" ClassName="Authorization" %>

<script runat="server">

/*

    This user control redirects current user to unauthorized.aspx page if the user does not have any of the role required for the folder.
    Roles required for a folder is determined from the appSetting "RequiredRoles" of the web.config file located in the folder
    This user control is housed in the master page.


*/


protected void RedirectToDefaultPage_Init(object sender, System.EventArgs e)
{
	// if user is already on unauthorized.aspx then exit
	if (Request.Path.Substring(Request.Path.LastIndexOf("/") + 1).Equals("unauthorized.aspx", StringComparison.OrdinalIgnoreCase))
		return;

	// Test roles
	if (CommonCode.GetVconfig("RequiredRoles") == null || string.IsNullOrWhiteSpace(CommonCode.GetVconfig("RequiredRoles")))
		return; 	// no access defined, so anybody can access
    
	foreach (string requiredRole in CommonCode.GetVconfig("RequiredRoles").Split(','))
    {
		if (Permissions.IsInRole(requiredRole)) return;
	}

	// if the user gets here then user does not have the necessary role: redirect him to unauthorized page
	Response.Redirect("~/NotificationPages/unauthorized.aspx");

}

</script>

<asp:Label ID="RedirectToDefaultPage" runat="server" EnableViewState="False" 
    oninit="RedirectToDefaultPage_Init"></asp:Label>

