﻿<%@ Control Language="C#" ClassName="SiteMapMenuImage" EnableViewState="false"   %>

<script runat="server">
    /*
     A wrapper for Menu control using Site Map
     This implementation renders menu as horizontally arranged icons. Please use 32 x 32 images.
     Provide the Site Map Provider name for property SiteMapProvider
     
     Additionally this wrapper hides the sub menu items marked with attribute Manu="HideOnProduction" when CommonCode.IsProduction()
     
     Denny Jacob - 12/30/2016 
     Last modified by Denny Jacob on 12/14/2017 
      
    */
    public string SiteMapProvider { get; set; } // this the only property that needs to be set where this control is used

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Menu1Data.SiteMapProvider = this.SiteMapProvider;
           
    }

    // Event truncates site map nodes with attribute Menu="Hide" 
    protected void Menu1_MenuItemDataBound(object sender, MenuEventArgs e)
    {
        var node = e.Item;
        var curNode = (System.Web.SiteMapNode) node.DataItem;

        // @"~/App_Images/Framework_Images/PagerRed.png" is the default image if one is not specified in ImageUrl attribute of this menu item in the site map file
        node.ImageUrl = curNode["ImageUrl"] != null ? curNode["ImageUrl"] : @"~/App_Images/Framework_Images/PagerRed.png";
        node.Text = "<br/>" + curNode.Title;

        if (curNode["Menu"] != null)
        {
            if (curNode["Menu"].Equals("Hide", System.StringComparison.OrdinalIgnoreCase))
                TruncateNode(sender, node);


            if (curNode["Menu"].Equals("HideOnProduction", System.StringComparison.OrdinalIgnoreCase) && CommonCode.IsProduction())
                TruncateNode(sender, node);

        }

    }

    private void TruncateNode(object sender, MenuItem node)
    {
        if (node.Parent == null)
        {
            var menu = (System.Web.UI.WebControls.Menu)sender;
            menu.Items.Remove(node);
        }
        else
        {
            node.Parent.ChildItems.Remove(node);
        }

    }

</script> 

<style type="text/css">

.Menu
{
    text-align: center;
    vertical-align: middle;
}

</style>

<asp:Menu ID="Menu1" runat="server" DataSourceID="Menu1Data"  CssClass="Menu"  
    Orientation="Horizontal" OnMenuItemDataBound="Menu1_MenuItemDataBound" 
    Font-Size="X-Small"  >

    <StaticMenuItemStyle Width="1.5in" />


</asp:Menu>

<asp:SiteMapDataSource ID="Menu1Data" runat="server" ShowStartingNode="false" />