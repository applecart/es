﻿<%@ Control Language="C#" ClassName="SiteMapMenu" EnableViewState="false"   %>

<script runat="server">
    /*
     A wrapper for Menu control using Site Map
     Provide the Site Map Provider name for property SiteMapProvider
     
     Additionally this wrapper hides the sub menu items marked with attribute Manu="Hide" 
        and hides the sub menu items marked with attribute Manu="HideOnProduction" when CommonCode.IsProduction()
     
     Denny Jacob - 12/30/2016 
     Last modified by Denny Jacob on 12/14/2017 
      
    */
    public string SiteMapProvider { get; set; } // this the only property that needs to be set where this control is used

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Menu1Data.SiteMapProvider = this.SiteMapProvider;
           
    }

    // Event truncates site map nodes with attribute Menu="Hide" 
    protected void Menu1_MenuItemDataBound(object sender, MenuEventArgs e)
    {
        var node = e.Item;
        var curNode = (System.Web.SiteMapNode)node.DataItem;

        if (curNode["Menu"] != null)
        {
            if (curNode["Menu"].Equals("Hide", System.StringComparison.OrdinalIgnoreCase))
                TruncateNode(sender, node);


            if (curNode["Menu"].Equals("HideOnProduction", System.StringComparison.OrdinalIgnoreCase) && CommonCode.IsProduction()) 
                TruncateNode(sender, node);

        }
        
    }

    private void TruncateNode(object sender, MenuItem node)
    {
        if (node.Parent == null)
        {
            var menu = (System.Web.UI.WebControls.Menu) sender;
            menu.Items.Remove(node);
        }
        else
        {
            node.Parent.ChildItems.Remove(node);
        }
        
    }

</script> 

<style type="text/css">

.Menu
{
    text-align: center;
    vertical-align: middle;
}

.level1
{
    display: block;
    border-radius: 2em 0em 0em 2em;
    margin-bottom: 2px;
}

</style>

<asp:Menu ID="Menu1" runat="server" DataSourceID="Menu1Data"  CssClass="Menu"  
    Orientation="Vertical" OnMenuItemDataBound="Menu1_MenuItemDataBound"  >

    <StaticMenuItemStyle BackColor="Black" BorderColor="White" BorderStyle="Solid" 
    BorderWidth="1px" ForeColor="White" VerticalPadding="9px" Width="1.5in" />

    <StaticHoverStyle BackColor="White" BorderColor="Black" BorderStyle="Solid" 
    BorderWidth="1px" ForeColor="Black" Width="1.5in" />

    <StaticSelectedStyle BackColor="White" BorderColor="Black" BorderStyle="Solid" 
    BorderWidth="1px" ForeColor="Black" VerticalPadding="9px" Width="1.5in" />

</asp:Menu>

<asp:SiteMapDataSource ID="Menu1Data" runat="server" ShowStartingNode="false" />
