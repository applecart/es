﻿<%@ Control Language="C#" ClassName="enumber" %>

<script runat="server">

public bool Enabled {
	get { return this.EmployeeNumber.Enabled; }
	set { this.EmployeeNumber.Enabled = value; }
    
}

public string Text {
	get { return this.EmployeeNumber.Text.ToUpperInvariant(); }
	set { this.EmployeeNumber.Text = value; }
    
}

public bool IsRequired {
	get { return this.RequiredFieldValidator1.Visible; }
	set { this.RequiredFieldValidator1.Visible = value; }
    
}

public override void Focus()
{
	this.EmployeeNumber.Focus();
    
}

    
</script>
<asp:TextBox ID="EmployeeNumber" runat="server" Width="0.7in" 
    ToolTip="Coca-Cola Employee Number e.g. T12345 or S216289" MaxLength="10"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
    ControlToValidate="EmployeeNumber" Display="Dynamic" ForeColor="Red" 
    ToolTip="Coca-Cola Employee Number e.g. T12345" 
    ValidationExpression="^[a-zA-Z0-9'@&#.\s]{6,10}$">*</asp:RegularExpressionValidator>

<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
    ControlToValidate="EmployeeNumber" Display="Dynamic" ForeColor="Red" 
    ToolTip="Employee number is required.">*</asp:RequiredFieldValidator>


