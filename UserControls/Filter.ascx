<%@ Control Language="C#" ClassName="Filter" %>

<script runat="server">

    /*
     
    Custom control facilitates search on forms containing data controls like GridView.
    It sets session variable "ExactFilter" initially to string.Empty 
        On clicking the Search button, this session variable is set to the literal that the user entered in the search box.
           (associate Session["ExactFilter"] to = @parameter of a query e.g. AND ID = @ExactFilter)
    Another session variable "LikeFilter" is set initially to "%"
        On clicking the Search button, this session variable is set to the literal (embedded between %) that the user entered in the search box.
           (associate Session["LikeFilter"] to LIKE @parameter of a query e.g. AND NAME LIKE @LikeFilter)
    NewSearchEvent: This custom event can be set to fire whenever ResetSearch() method is called by wiring it at Page_Load event as follows 
        this.Filter1.NewSearchEvent += new ASP.Filter.NewSearchEventHandler(NewSearchEvent); // write your custom code in NewSearchEvent()

*/

    // Delegate method for NewSearchEvent. Provide param signatures as needed.
    public delegate void NewSearchEventHandler();

    // Event handler delcaration for NewSearchEvent.
    public event NewSearchEventHandler NewSearchEvent;

    protected void Search_Click(object sender, System.EventArgs e)
    {
	    if (string.IsNullOrWhiteSpace(txtFilter.Text)) 
        {
		    ResetSearch();
	    } 
        else 
        {
		    Session["ExactFilter"] = txtFilter.Text.Trim();
		    Session["LikeFilter"] = "%" + Session["ExactFilter"].ToString()  + "%";
		    if (NewSearchEvent != null) NewSearchEvent();
	    }
        
    }

    protected void Clear_Click(object sender, System.EventArgs e)
    {
	    ResetSearch();
        
    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
	    if (Session["ExactFilter"] == null) ResetSearch();

    }

    protected void Page_PreRender(object sender, System.EventArgs e)
    {
	    TieButton(txtFilter, Search);
	    txtFilter.Text = Session["ExactFilter"].ToString();
	    txtFilter.Focus();
    }

    public void ResetSearch()
    {
	    Session["ExactFilter"] = " ";
	    Session["LikeFilter"] = "%";
	    if (NewSearchEvent != null) NewSearchEvent();
        
    }

    private void TieButton(TextBox oTextBox, Button oButton)
    {
	    string jsString = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {document." + "forms[0].elements['" + oButton.UniqueID.Replace(":", "_") + "'].click();return false;} else return true; ";
	    oTextBox.Attributes.Add("onkeydown", jsString);
    }

</script>

<asp:TextBox ID="txtFilter" runat="server" 
    ToolTip="Type in word(s) to filter the displayed list" EnableViewState="False" 
    CssClass="HideOnPreview"></asp:TextBox>
<asp:Button CssClass="Selected HideOnPreview Search" ID="Search" runat="server"
    Text="Search" ToolTip="Click here to filter records by given words(s)" EnableViewState="False"
    CausesValidation="False" UseSubmitBehavior="False" onclick="Search_Click"/>
<asp:Button CssClass="Selected HideOnPreview Clear" ID="Clear" runat="server" 
    Text="Clear" ToolTip="Reset and clear the search" EnableViewState="False" 
    CausesValidation="False" UseSubmitBehavior="False" onclick="Clear_Click" />
