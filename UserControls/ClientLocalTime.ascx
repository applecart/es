<%@ Control Language="C#" ClassName="ClientLocalTime" %>

<script runat="server">

/*
    The primary advantage of storing date/time values in UTC is that it makes the data transportable.
    To see what I mean, imagine that following scenario: you have an eCommerce website that is being
    hosted in a web server located in the Pacific time zone (UTC -8) and this application stores the 
    date and time orders were placed in server time. Say a user, Bob, makes an order on August 1, 2007 at 9:00 AM UTC -8. 
    
    After many months of phenomenal growth, you decide to switch to a larger web hosting company, 
    one on the east coast where the time zone is UTC -5. 
    
    Since the date/time is stored in server time, Bob`s previous order still shows that it was made on August 1 2007 at 9:00 AM. 
    But since we are now in UTC -5, it is as if Bob`s order was made three hours earlier than it really was 
    (since when it was 9:00 AM on August 1, 2007 in the west coast it was really 12:00 noon on the east coast). 

    - Scott Mitchell
*/
    

    /// Generates a filename consisting of time at the client, to 100th of the second.
    public string GetBackupFilename
    {
	    get { return GetLocalDt(DateTime.UtcNow).ToString("yyyyMMdd-HHmm-ssf"); }
        
    }

    /// Converts a DateTime value that the client enters to UTC DateTime equivalent
    public DateTime GetLocalDtUtc(DateTime localDt)
    {
	    return localDt.AddMinutes(GetOffsetMinutes());
        
    }

    /// Converts a UTC DateTime value to client's local DateTime equivalent value
    public DateTime GetLocalDt(DateTime utcDt)
    {
	    return utcDt.AddMinutes(-1 * GetOffsetMinutes());
         
    }

    /// Minutes offset between client's local time and UTC
    public int GetOffsetMinutes()
    {
	    return int.Parse(OffsetMinutes.Value.ToString());
         
    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
	    if (!IsPostBack) 
        {
		    Page.ClientScript.RegisterStartupScript(typeof(string)
                , "OffsetFromUTC"
                , "OffsetFromUTC('" + OffsetMinutes.ClientID + "');"
                , true);
	    }
        
    }
 
</script>

<input id="OffsetMinutes" runat="server" type="hidden" />

<script type="text/javascript">
    function OffsetFromUTC(id) { document.getElementById(id).value = (new Date()).getTimezoneOffset(); }
</script>