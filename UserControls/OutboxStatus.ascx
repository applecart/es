﻿<%@ Control Language="C#" ClassName="OutboxStatus" %>

<script runat="server">
    
/*    
    User control to adds comma separated statuses defined
        in the web.config > appSettings > OutboxStatuses to its radio button list interface.
    Text property can be used to Get/Set the selected value on the list.
    Enabled: property enables/disables the selection
    
*/
    

public string Text {
	get { return this.CurrentStatus.SelectedValue; }
	set { this.CurrentStatus.SelectedValue = value; }
    
}


public bool Enabled {
	get { return this.CurrentStatus.Enabled; }
	set { this.CurrentStatus.Enabled = value; }
    
}


protected void Page_Init(object sender, System.EventArgs e)
{
	if (this.CurrentStatus.Items.Count > 0) return;
	foreach (string status in CommonCode.GetVconfig("OutboxStatuses").Split(',')) {
		this.CurrentStatus.Items.Add(status.Trim());
	}

}

   
</script>

<style type="text/css">
    table.NoWrapRadio td {white-space:nowrap;}
    .ThickBorder {border: 2px solid red;}
</style>

<table><tr><td>
<fieldset class="ThickBorder" >
<legend>Manage status:</legend> 
<asp:RadioButtonList ID="CurrentStatus" runat="server" CellPadding="5"  RepeatDirection="Horizontal"  CssClass="NoWrapRadio" />
</fieldset>
</td></tr></table>
