﻿<%@ Control Language="C#" ClassName="FadingLabel"  AutoEventWireup="false" EnableViewState="false" %>

<script type='text/javascript'>

    function fadeOut(clientID) {
        var element = document.getElementById(clientID);
        var op = 1;  // initial opacity
        var timer = setInterval(function () {
            if (op < 0.1) {
                clearInterval(timer);
                element.style.display = 'none';
            }
            element.style.opacity = op;
            element.style.filter = 'alpha(opacity=' + op * 100 + ')';
            op -= op * 0.1;
        }, 300);
    }

</script>

<script runat="server">

    /*

    A control to show text which will fade-out gradually.
    This control has only two properties as described below:
    FadingLabelProperties: To set properties like ForeColor, Font etc. Do not set Text property here, instead use FadingText for that.
    FadingText: To set the text that fades out.

    Denny Jacob - 08/21/2016
    Last modified on - 08/31/2016

    */
    
    public Label FadingLabelProperties
    {
        get { return this.MyLabelControl; }
    }

    public string FadingText
    {
        get { return null; }
        set
        {
            this.MyLabelControl.Text = value;
            this.JsTrigger.Text = string.Format(@"<script type='text/javascript'>fadeOut('{0}');<{1}script>", this.MyLabelControl.ClientID, "/");
        }
    }
    
</script>

<asp:Label ID="MyLabelControl" runat="server" ForeColor="Green" />

<asp:Literal ID="JsTrigger" runat="server" />
