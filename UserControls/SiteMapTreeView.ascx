﻿<%@ Control Language="C#" ClassName="SiteMapTreeView" %>

<script runat="server">
    /*
     A wrapper for TreeView control using Site Map
     Provide the Site Map Provider name for property SiteMapProvider
     
     Additionally this wrapper hides the sub menu items marked with attribute Manu="Hide" 
        and hides the sub menu items marked with attribute Manu="HideOnProduction" when CommonCode.IsProduction()
     
     Denny Jacob - 12/31/2016 
     Last modified by Denny Jacob on 12/14/2017 
      
    */
    public string SiteMapProvider { get; set; } // this the only property that needs to be set where this control is used

    protected void Page_PreRender(object sender, EventArgs e)
    {
        TreeView1Data.SiteMapProvider = this.SiteMapProvider;

    }

    // Event truncates site map nodes with attribute Menu="Hide" 
    protected void TreeView1_TreeNodeDataBound(object sender, System.Web.UI.WebControls.TreeNodeEventArgs e)
    {
        var node = e.Node;
        var curNode = (System.Web.SiteMapNode)node.DataItem;

        if (curNode["Menu"] != null)
        {
            if (curNode["Menu"].Equals("Hide", System.StringComparison.OrdinalIgnoreCase))
                TruncateNode(sender, node);


            if (curNode["Menu"].Equals("HideOnProduction", System.StringComparison.OrdinalIgnoreCase) && CommonCode.IsProduction())
                TruncateNode(sender, node);

        }

    }

    private void TruncateNode(object sender, TreeNode node)
    {
        if (node.Parent == null)
        {
            var menu = (System.Web.UI.WebControls.TreeView) sender;
            menu.Nodes.Remove(node);
        }
        else
        {
            node.Parent.ChildNodes.Remove(node);
        }

    }

</script>

<asp:TreeView ID="TreeView1" runat="server" DataSourceID="TreeView1Data" ShowExpandCollapse="false"
    CssClass="HideOnPreview" NodeIndent="9" 
    OnTreeNodeDataBound="TreeView1_TreeNodeDataBound">
    <HoverNodeStyle BackColor="Red" ForeColor="White" />
    <SelectedNodeStyle ForeColor="Red" />
</asp:TreeView>
<asp:SiteMapDataSource ID="TreeView1Data" runat="server" ShowStartingNode="False"  />

