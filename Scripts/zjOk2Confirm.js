/*
These javascript functions seek confirmation on button / link button control that has the word 'confirm' anywhere in its ID tag.
It then prevents a postback in the event that the user opts to [Cancel].

For a button / link button control that does not have the word 'confirm' in its ID tag, simply set its OnClientClick property as shown.
    The test for IsPageValid() and/or HasUserConfirmed() are optional but use them in the order shown.
        OnClientClick="if(IsPageValid() && HasUserConfirmed()){ ApplyProcessingStyle(this); } else { return false; }" 
    Submit behavior should be false when the control needs to be disabled for the duration of the process.
        UseSubmitBehavior="False"
        OnClientClick="this.disabled='true';ApplyProcessingStyle(this);" 

<script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/zjOk2Confirm.js") %> "></script> 
*/

function confirmPostback(e) {
    var targ;
    if (!e) var e = window.event;
    targ = (e.target) ? e.target : e.srcElement;
    if (targ.nodeType == 3) targ = targ.parentNode
    {
        if (targ.id.toLowerCase().indexOf("confirm") >= 0) {
            if (HasUserConfirmed()) {
                ApplyProcessingStyle(targ);
                return true;
            }
            else { return false; }
            routeEvent(e);
        }
    }
}

document.onclick = confirmPostback;

function IsPageValid() {
    if (typeof Page_ClientValidate === "undefined") {
        return true;
    } else {
        Page_ClientValidate();
        return Page_IsValid;
    }
}

function HasUserConfirmed() {
    return confirm("Please select [Ok] to confirm...");
}

function ApplyProcessingStyle(oThis) {
    oThis.value = 'Processing...';
    oThis.innerHTML = oThis.value;
    // oThis.disabled = true;
    oThis.style.backgroundColor = '#FFFF00';
}
