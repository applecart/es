

public partial class frmLISTtemplate : System.Web.UI.Page
{

#region "User defined methods" // Please define all your custom methods in this region

#endregion

#region "Template methods - Hooks"

    private void PreBindInitializeOnlyOnce()
    {
        // NOTE: This method gets called only on first page load - not on post back(s).
        // Place code here to reset filter or to supply query parameters.
        if (Request.UrlReferrer == null || !Request.UrlReferrer.ToString().Contains("Edit.aspx")) this.Filter1.ResetSearch();

    }

    private void PostBindInitializeOnlyOnce()
    {
        // NOTE: This method gets called only on first page load - not on post back(s).
        // Place code here to initialize/restore state of data bound controls on the form.

    }

    private void PostBindInitializeAlways()
    {
        // NOTE: This method gets called every time this page loads.
        // Place code here to influence state of controls which depend on other databound control(s) on the form, always.

    }

#endregion

#region "Template methods - Standard"

    protected void NewSearchEvent()
    {
        if (this.grdView.AllowPaging)
            grdView.PageIndex = 0;

    }

    private void RetrieveBookmarkedGridViewPage()
    {
        if (!grdView.AllowPaging || Session["My.CurrentPageIndex"] == null || !Session["My.CurrentPagePath"].ToString().Equals(Request.Path, System.StringComparison.OrdinalIgnoreCase))
        {
            // cannot retrieve bookmark
        }
        else
        {
            grdView.PageIndex = (int) Session["My.CurrentPageIndex"];
        }

    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.Filter1.NewSearchEvent += new ASP.Filter.NewSearchEventHandler(NewSearchEvent);

    }

    protected void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Redirect("~/NotificationPages/Unauthorized.aspx");
        if (!IsPostBack)
            PreBindInitializeOnlyOnce();

    }

    protected void Page_PreRenderComplete(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            RetrieveBookmarkedGridViewPage();
            PostBindInitializeOnlyOnce();
            // avoids initialization on postbacks
        }
        PostBindInitializeAlways();

    }

    protected void Page_SaveStateComplete(object sender, System.EventArgs e)
    {
        Session["My.CurrentPageIndex"] = grdView.PageIndex;
        // bookmark current page
        Session["My.CurrentPagePath"] = Request.Path;

    }


#endregion

}

