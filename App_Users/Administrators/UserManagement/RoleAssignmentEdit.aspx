<%@ Page Language="C#" validateRequest="false" MasterPageFile="~/zmSite01.master" CodeFile="RoleAssignmentEdit.aspx.cs" Inherits="frmDEtemplate" title="Data Entry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table cellpadding="0" cellspacing="0" align="center">
        <tr valign="top" >
            <td class="td-filler"></td>
            <td align="left" colspan="3" nowrap="nowrap" >
                <asp:ValidationSummary ID="valSummary" runat="server" ForeColor="Red" />
            </td>
        </tr>
        <tr valign="top">
            <td class="td-filler"></td>
            <td class="td-bgcolor" align="left" colspan="3" nowrap="nowrap">
                <table align="center">
                    <tr valign="top" >
                        <td style="text-align: left;" colspan="2">
                            <asp:Literal ID="Legend" runat="server" Text="Role(s) not yet assigned to {0}"></asp:Literal>
                        </td>
                    </tr>
                    <tr valign="top" >
                        <td style="text-align: left;" rowspan="2">
                            <asp:GridView ID="UnassignedRoles" runat="server" AutoGenerateColumns="False" 
                                DataSourceID="RoleAssignmentData" HeaderStyle-CssClass="GridViewHeader" 
                                EmptyDataText="User already has all available roles"  >
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="IsSelected" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RoleName" SortExpression="RoleName">
                                        <ItemTemplate>
                                            <asp:Label ID="RoleName" runat="server" Text='<%# Bind("RoleName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="RoleDescription" HeaderText="RoleDescription" 
                                        SortExpression="RoleDescription" >
                                    </asp:BoundField>
                                </Columns>

<HeaderStyle CssClass="GridViewHeader"></HeaderStyle>
                            </asp:GridView>
                            <asp:SqlDataSource ID="RoleAssignmentData" runat="server" 
                                
                                ConnectionString="<%$ ConnectionStrings:NIBFC %>" SelectCommand="SELECT
	rls.RoleName 
               ,rls.RoleDescription
FROM
               dbo.tbl_NIBFC_Roles rls
WHERE
                rls.RoleName NOT IN
(
SELECT
	asn.RoleName
FROM
	dbo.tbl_NIBFC_RoleAssignments	asn
INNER JOIN
    dbo.tbl_NIBFC_Users usr
ON
    asn.Enumber = usr.Enumber
WHERE
	usr.ID = @ID
)
ORDER BY
1">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="ID" QueryStringField="UserId" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td style="width: 100px;">
                            &nbsp;</td>
                    </tr>
                     <tr valign="top" >
                        <td style="width: 100px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td class="td-filler"></td>
            <td class="td-bgcolor" align="left" nowrap="nowrap" style="width: 250px;">
                <asp:HyperLink CssClass="Selected HideOnPreview PreviousPage" ID="PreviousPageLink" 
                    runat="server" NavigateUrl="~/Default.aspx" 
                    ToolTip="Click here to return to the previous page">Previous page</asp:HyperLink></td>
            <td class="td-bgcolor" align="left" nowrap="nowrap" style="width: 250px;">
                <asp:Button
                    CssClass="Selected HideOnPreview Save"
                    ID="Edit"
                    runat="server"
                    Text="Add"
                    ToolTip="Add this record"
                    OnClick="Edit_Click" 
                    CausesValidation="false" 
                    OnClientClick="if(IsPageValid()){ ApplyProcessingStyle(this); } else { return false; }"
                    /></td>
            <td class="td-bgcolor" align="left" nowrap="nowrap" style="width: 250px;">
                <asp:Button CssClass="Selected HideOnPreview Delete" ID="ConfirmThenDelete" 
                    runat="server" CausesValidation="False"
                    Text="Delete" Visible="False" ToolTip="Delete this record" onclick="ConfirmThenDelete_Click" />
             </td>
        </tr>
    </table>
</asp:Content>

