
// Data Entry template
partial class frmDEtemplate : System.Web.UI.Page
{

#region "User defined methods" - Please define all your custom methods in this region

    protected void Find_Click(object sender, System.EventArgs e)
    {
        // Try resolving the employee name 
        this.DisplayName.Text = Permissions.EmployeeName(this.Enumber.Text);

    }

#endregion

#region "Template methods - Hooks"

	private void PrepFormControls()
	{
		// Called from Page_PreRender routine provides data for controls like DropDownList(s)
		// This is also a good place to direct focus to the first input control on Add and on Edit e.g: txtName.Focus()
        if (IsModeInsert()) this.Enumber.Focus(); else this.DisplayName.Focus();    
	}

	private void PopulateFormAdd()
	{
		// Called from Page_PreRenderComplete and from Edit_Click, routine populates this fields on the form for an insert record operation

	}

	private void PopulateFormEdit()
	{
		// Called from Page_PreRenderComplete, routine populates the fields on this form for an update/delete record operation
        this.Enumber.Enabled = false;
        this.Find.Enabled = false;
        var sql = @"SELECT * FROM dbo.tbl_NIBFC_Users WHERE ID = @ID";

        using (var rs = new Framework_Code.DataAccess("NIBFC"))
        {
            rs.SetParameterValue("ID", GetRecId());
            var row = rs.ExecuteSql(sql).Rows[0];
            this.Enumber.Text = row["Enumber"].ToString();
            this.DisplayName.Text = row["DisplayName"].ToString();
            this.Company.SelectedValue = row["CompanyCode"].ToString();
        }

	}

	private bool IsCancelRecordInsert()
	{
		// Called from from Edit_Click, routine inserts a record in the appropriate table
        // Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).

        var isDuplicateUser = false;
        var sql =
        @"
        INSERT INTO [dbo].[tbl_NIBFC_Users]
               ([Enumber]
               ,DisplayName
               ,CompanyCode
               )
         VALUES
               (
                @Enumber
               ,@DisplayName
               ,@CompanyCode
               )  
        ";

        var testUniqueSql =
            @"SELECT COUNT(*) FROM dbo.tbl_NIBFC_Users WHERE ENumber = @Enumber";

        using (var rs = new Framework_Code.DataAccess("NIBFC"))
        {
            rs.SetParameterValue("Enumber", this.Enumber.Text);
            isDuplicateUser =  (int) rs.ExecuteScalar(testUniqueSql) != 0;
            if (isDuplicateUser) 
               this.Message.Text = "This user is already added.";
            else
            {
                rs.SetParameterValue("Enumber",this.Enumber.Text);
                rs.SetParameterValue("DisplayName", this.DisplayName.Text);
                rs.SetParameterValue("CompanyCode", string.IsNullOrWhiteSpace(this.Company.SelectedValue) ? null : this.Company.SelectedValue);
                rs.ExecuteNonQuery(sql);
           }

        }

        return isDuplicateUser;

	}

	private bool IsCancelRecordUpdate()
	{
		// Called from from Edit_Click, routine updates a record in the appropriate table
		// Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).
        var sql = 
        @"
        UPDATE [dbo].[tbl_NIBFC_Users]
           SET 
               DisplayName = @DisplayName
             , CompanyCode = @CompanyCode
         WHERE 
	        ID = @ID
        ";

        using (var rs = new Framework_Code.DataAccess("NIBFC"))
        {
            rs.SetParameterValue("ID", GetRecId());
            rs.SetParameterValue("DisplayName", this.DisplayName.Text);
            rs.SetParameterValue("CompanyCode", string.IsNullOrWhiteSpace(this.Company.SelectedValue) ? null : this.Company.SelectedValue);
            rs.ExecuteNonQuery(sql);
        }

        return false;

	}

	private bool IsCancelRecordDelete()
	{
		// Called from from ConfirmThenDelete_Click, routine flags a record in the appropriate table
		// Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).
        var sql =
        @"
         DELETE FROM dbo.tbl_NIBFC_Users WHERE ID = @ID
        ";

        using (var rs = new Framework_Code.DataAccess("NIBFC"))
        {
            rs.SetParameterValue("ID", GetRecId());
            rs.ExecuteNonQuery(sql);
        }

		return false;

	}

	private bool IsModeInsert()
	{
		return Request.QueryString.Count == 0;

	}

	private int GetRecId()
	{
		return System.Convert.ToInt32(Request.QueryString[0]);

	}

#endregion
    /*
      --------------------------------------------------------------------------
      NOTE: ALTERING ANY METHOD BELOW THIS LINE WILL RESULT IN NON-CONFORMANCE
      --------------------------------------------------------------------------
    */
#region "Template methods - Standard"

	protected void Page_PreRender(object sender, System.EventArgs e)
	{
		PrepFormControls();

	}

	protected void Page_PreRenderComplete(object sender, System.EventArgs e)
	{
		if (!IsPostBack) {
			PreviousPageLink.NavigateUrl = Request.UrlReferrer.ToString();
			if (IsModeInsert()) {
				// Insert request
				PopulateFormAdd();
			} else {
				// Edit request for Update or Delete
				ConfirmThenDelete.Visible = true;
				Edit.Text = "Update";
				Edit.ToolTip = "Update this record";
				lblRecID.Text = GetRecId().ToString();
				PopulateFormEdit();
			}
		}

	}

	protected void Edit_Click(object sender, System.EventArgs e)
	{
        if (IsModeInsert())
        {
            if (!IsCancelRecordInsert()) Response.Redirect(PreviousPageLink.NavigateUrl);
        }
        else
        {
            if (!IsCancelRecordUpdate()) Response.Redirect(PreviousPageLink.NavigateUrl);
        }

        // Problem - form stays

    }

	protected void ConfirmThenDelete_Click(object sender, System.EventArgs e)
	{
		if (!IsCancelRecordDelete()) Response.Redirect(PreviousPageLink.NavigateUrl);

        // Problem - form stays

	}

#endregion

}

