
// Data Entry template
partial class frmDEtemplate : System.Web.UI.Page
{

#region "User defined methods" - Please define all your custom methods in this region

    private void UpdateLegend()
    {
        using (var rs = new Framework_Code.DataAccess("NIBFC"))
        {
            rs.SetParameterValue("ID", GetRecId());
            this.Legend.Text =
                string.Format(this.Legend.Text
                    , rs.ExecuteScalar(@"SELECT DisplayName + ' (' + Enumber + ')' FROM dbo.tbl_NIBFC_Users WHERE ID = @ID")
                    );

        }

    }

    private string ResolveEnumber(int userId)
    {
        using (var rs = new Framework_Code.DataAccess("NIBFC"))
        {
            rs.SetParameterValue("ID", userId);
            return (string) rs.ExecuteScalar(@"SELECT Enumber FROM dbo.tbl_NIBFC_Users WHERE ID = @ID");

        }
    }

#endregion

#region "Template methods - Hooks"

	private void PrepFormControls()
	{
		// Called from Page_PreRender routine provides data for controls like DropDownList(s)
		// This is also a good place to direct focus to the first input control on Add and on Edit e.g: txtName.Focus()

	}

	private void PopulateFormAdd()
	{
		// Called from Page_PreRenderComplete and from Edit_Click, routine populates this fields on the form for an insert record operation
        this.Edit.Enabled = this.UnassignedRoles.Rows.Count > 0;
        UpdateLegend();
 
	}

	private void PopulateFormEdit()
	{
		// Called from Page_PreRenderComplete, routine populates the fields on this form for an update/delete record operation

		// var row = Get1Record();

	}

	private bool IsCancelRecordInsert()
	{
		// Called from from Edit_Click, routine inserts a record in the appropriate table
        // Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).
        var sql =
        @"
         INSERT INTO [dbo].[tbl_NIBFC_RoleAssignments]
               ([Enumber]
               ,[RoleName])
         VALUES
               (
                @Enumber
               ,@RoleName
               )
        ";

        using (var rs = new Framework_Code.DataAccess("NIBFC"))
        {
            foreach (System.Web.UI.WebControls.GridViewRow row in this.UnassignedRoles.Rows)
            {
                var findControl = new Framework_Code.FindControls(row);
                if (findControl.ValueOfCheckBoxAsBoolean("IsSelected"))
                {
                    // selected - add this role
                    rs.SetParameterValue("Enumber", ResolveEnumber(GetRecId()));
                    rs.SetParameterValue("RoleName", findControl.ValueOfLabelAsString("RoleName"));
                    rs.ExecuteNonQuery(sql);
                }
            } 
        }

		return false;

	}

	private bool IsCancelRecordUpdate()
	{
		// Called from from Edit_Click, routine updates a record in the appropriate table
		// Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).

		return false;

	}

	private bool IsCancelRecordDelete()
	{
		// Called from from ConfirmThenDelete_Click, routine flags a record in the appropriate table
		// Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).

		return false;

	}

	private bool IsModeInsert()
	{
		return true;

	}

	private int GetRecId()
	{
		return System.Convert.ToInt32(Request.QueryString[0]);

	}

#endregion
    /*
      --------------------------------------------------------------------------
      NOTE: ALTERING ANY METHOD BELOW THIS LINE WILL RESULT IN NON-CONFORMANCE
      --------------------------------------------------------------------------
    */
#region "Template methods - Standard"

	protected void Page_PreRender(object sender, System.EventArgs e)
	{
		PrepFormControls();

	}

	protected void Page_PreRenderComplete(object sender, System.EventArgs e)
	{
		if (!IsPostBack) {
			PreviousPageLink.NavigateUrl = Request.UrlReferrer.ToString();
			if (IsModeInsert()) {
				// Insert request
				PopulateFormAdd();
			} else {
				// Edit request for Update or Delete
				ConfirmThenDelete.Visible = true;
				Edit.Text = "Update";
				Edit.ToolTip = "Update this record";
				PopulateFormEdit();
			}
		}

	}

	protected void Edit_Click(object sender, System.EventArgs e)
	{
        if (IsModeInsert())
        {
            if (!IsCancelRecordInsert()) Response.Redirect(PreviousPageLink.NavigateUrl);
        }
        else
        {
            if (!IsCancelRecordUpdate()) Response.Redirect(PreviousPageLink.NavigateUrl);
        }

        // Problem - form stays

    }

	protected void ConfirmThenDelete_Click(object sender, System.EventArgs e)
	{
		if (!IsCancelRecordDelete()) Response.Redirect(PreviousPageLink.NavigateUrl);

        // Problem - form stays

	}

#endregion

}

