
<%@ Page Language="C#" MasterPageFile="~/zmSite01.master" CodeFile="RoleAssignments.aspx.cs" Inherits="frmLISTtemplate"  Title="List Data" %>

<%@ Register Src="~/UserControls/Filter.ascx" TagName="Filter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table cellpadding="0" cellspacing="0" align="center">
        <tr valign="top">
            <td class="td-filler"></td>
            <td class="td-bgcolor" align="left" valign="bottom" nowrap="nowrap">
                        &nbsp;</td>
            <td class="td-bgcolor" align="left" valign="bottom" nowrap="nowrap" style="text-align: right;">
                <uc1:Filter ID="Filter1" runat="server" Visible="False" />
            </td>
        </tr>
        <tr valign="top">
            <td class="td-filler">&nbsp;</td>
            <td class="td-bgcolor" align="left" valign="bottom" nowrap="nowrap" colspan="2">
                <asp:Literal ID="Legend" runat="server" 
                    Text="Currently assigned role(s) to {0}"></asp:Literal>
            </td>
        </tr>
        <tr valign="top">
            <td class="td-filler"></td>
            <td class="td-bgcolor" align="left" colspan="2" nowrap="nowrap">
                <asp:GridView ID="grdView" runat="server" AllowPaging="True" 
                    AllowSorting="True" HeaderStyle-CssClass="GridViewHeader"  
                    CellPadding="3" EmptyDataText="No data to list" EnableViewState="False"
                    GridLines="Vertical" ToolTip="Click on header to sort by that column" 
                    AutoGenerateColumns="False" DataKeyNames="ID" 
                    DataSourceID="RoleAssignmentData" BackColor="White" BorderColor="#999999" 
                    BorderStyle="None" BorderWidth="1px">
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <Columns>
                        <asp:TemplateField InsertVisible="False" SortExpression="ID">
                            <ItemTemplate>
                                <asp:LinkButton ID="RemoveConfirm" runat="server" 
                                    CommandArgument='<%# Eval("ID") %>' onclick="RemoveConfirm_Click">Remove</asp:LinkButton>
                            </ItemTemplate>
                            <ControlStyle CssClass="HideOnPreview Selected Delete" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="RoleName" HeaderText="RoleName" 
                            SortExpression="RoleName" />
                        <asp:BoundField DataField="RoleDescription" HeaderText="RoleDescription" 
                            SortExpression="RoleDescription" />
                    </Columns>

                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#DCDCDC" />
    
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
    
                </asp:GridView>
                <asp:SqlDataSource ID="RoleAssignmentData" runat="server" 
                    
                    
                    ConnectionString="<%$ ConnectionStrings:NIBFC %>" SelectCommand="SELECT
	asn.*
            ,   rls.RoleDescription
FROM
	dbo.tbl_NIBFC_RoleAssignments	asn
INNER JOIN
                dbo.tbl_NIBFC_Roles rls
ON
                 asn.RoleName = rls.RoleName
INNER JOIN
                 dbo.tbl_NIBFC_Users usr
ON
                 asn.Enumber = usr.Enumber
WHERE
	 usr.ID = @ID
ORDER BY
                  asn.RoleName">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="ID" QueryStringField="UserId" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr valign="top">
            <td class="td-filler"></td>
            <td class="td-bgcolor" align="left" nowrap="nowrap">
                <asp:HyperLink CssClass="Selected HideOnPreview PreviousPage" ID="PreviousPageLink" 
                    runat="server" NavigateUrl="Users.aspx" 
                    ToolTip="Click here to return to the previous page">Previous page</asp:HyperLink>
            </td>
            <td class="td-bgcolor" align="left" nowrap="nowrap" >
                <asp:HyperLink CssClass="Selected HideOnPreview Add" ID="AddNewRecords" 
                    runat="server" NavigateUrl="RoleAssignmentEdit.aspx?UserId={0}" 
                    ToolTip="Click here for data entry form">Add role(s)</asp:HyperLink>
            </td>
        </tr>
    </table>

</asp:Content>

