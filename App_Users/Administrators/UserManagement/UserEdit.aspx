<%@ Page Language="C#" validateRequest="false" MasterPageFile="~/zmSite01.master" CodeFile="UserEdit.aspx.cs" Inherits="frmDEtemplate" title="Data Entry" %>

<%@ Register src="~/UserControls/enumber.ascx" tagname="enumber" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table cellpadding="0" cellspacing="0" align="center">
        <tr valign="top" >
            <td class="td-filler"></td>
            <td align="left" colspan="3" nowrap="nowrap" >
                <asp:ValidationSummary ID="valSummary" runat="server" ForeColor="Red" />
            </td>
        </tr>
        <tr valign="top">
            <td class="td-filler"></td>
            <td class="td-bgcolor" align="left" colspan="3" nowrap="nowrap">
                <table align="center">
                    <tr valign="top" >
                        <td style="text-align: right;">Record#</td>
                        <td style="width: 110px;" >
                            <asp:Label ID="lblRecID" runat="server" Text="New Record"></asp:Label>
                         </td>
                        <td style="width: 100px;"></td>
                    </tr>
                    <tr valign="top" >
                        <td style="text-align: right;">Enumber:</td>
                        <td >
                            <uc1:enumber ID="Enumber" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="Find" runat="server" CausesValidation="False" Text="Find" OnClick="Find_Click" CssClass="HideOnPreview Selected Search"   />
                        </td>
                    </tr>
                     <tr valign="top" >
                        <td style="text-align: right;">Employee name:</td>
                        <td style="width: 110px;">
                            <asp:TextBox ID="DisplayName" runat="server" MaxLength="50"></asp:TextBox>
                         </td>
                        <td style="width: 100px;">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="DisplayName" Display="Dynamic" ForeColor="Red">*</asp:RequiredFieldValidator>
                         </td>
                    </tr>
                     <tr valign="top" >
                        <td style="text-align: right;">Company:</td>
                        <td style="width: 110px;">
                            <asp:DropDownList ID="Company" runat="server" DataSourceID="ListCompaniesData" DataTextField="DisplayName" DataValueField="Code">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="ListCompaniesData" runat="server" ConnectionString="<%$ ConnectionStrings:NIBFC %>" SelectCommand="SELECT 
      [DisplayName]
     ,[Code]
FROM [dbo].[tbl_NIBFC_Companies]
ORDER BY 2  
"></asp:SqlDataSource>
                         </td>
                        <td style="width: 100px;">
                            &nbsp;</td>
                    </tr>
                     <tr valign="top" >
                        <td style="text-align: left;" colspan="3">
                            <asp:Label ID="Message" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label>
                         </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="top">
            <td class="td-filler"></td>
            <td class="td-bgcolor" align="left" nowrap="nowrap" style="width: 250px;">
                <asp:HyperLink CssClass="Selected HideOnPreview PreviousPage" ID="PreviousPageLink" 
                    runat="server" NavigateUrl="~/Default.aspx" 
                    ToolTip="Click here to return to the previous page">Previous page</asp:HyperLink></td>
            <td class="td-bgcolor" align="left" nowrap="nowrap" style="width: 250px;">
                <asp:Button
                    CssClass="Selected HideOnPreview Save"
                    ID="Edit"
                    runat="server"
                    Text="Add"
                    ToolTip="Add this record"
                    OnClick="Edit_Click" 
                    CausesValidation="false" 
                    OnClientClick="if(IsPageValid()){ ApplyProcessingStyle(this); } else { return false; }"
                    /></td>
            <td class="td-bgcolor" align="left" nowrap="nowrap" style="width: 250px;">
                <asp:Button CssClass="Selected HideOnPreview Delete" ID="ConfirmThenDelete" 
                    runat="server" CausesValidation="False"
                    Text="Delete" Visible="False" ToolTip="Delete this record" onclick="ConfirmThenDelete_Click" />
             </td>
        </tr>
    </table>
</asp:Content>

