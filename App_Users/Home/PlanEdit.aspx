<%@ Page Language="C#" validateRequest="false" MasterPageFile="~/zmSite01.master" CodeFile="PlanEdit.aspx.cs" Inherits="frmDEtemplate" title="Data Entry" %>


<asp:Content
    ID="Content1"
    ContentPlaceHolderID="ContentPlaceHolder1"
    runat="Server">
    <table
        cellpadding="0"
        cellspacing="0"
        align="center">
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                align="left"
                colspan="3"
                nowrap="nowrap">
                <asp:ValidationSummary
                    ID="valSummary"
                    runat="server"
                    ForeColor="Red" />
            </td>
        </tr>
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                class="td-bgcolor"
                align="left"
                colspan="3">
                <table
                    align="center">
                    <tr
                        valign="top">
                        <td
                            style="text-align: right;
                            background-color: #CC0000;
                            color: White;">
                            &nbsp;
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:Label
                                ID="lblRecID"
                                runat="server"
                                Text="New Record"></asp:Label>
                        </td>
                        <td
                            style="width: 100px;">
                        </td>
                    </tr>
                    <tr
                        valign="top">
                        <td
                            style="text-align: right;
                            background-color: #CC0000;
                            color: White;">
                            PlanName:
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:TextBox ID="PlanName" 
                                runat="server"></asp:TextBox>
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                runat="server" 
                                ControlToValidate="PlanName" 
                                Display="Dynamic" 
                                ErrorMessage="PlanName is required" 
                                ForeColor="Red">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr
                        valign="top">
                        <td
                            style="text-align: right;
                            background-color: #CC0000;
                            color: White;">
                            PlanFixedCost:</td>
                        <td
                            style="width: 100px;">
                            <asp:TextBox ID="PlanFixedCost" 
                                runat="server"></asp:TextBox>
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                runat="server" 
                                ControlToValidate="PlanFixedCost" 
                                Display="Dynamic" 
                                ErrorMessage="PlanFixedCost is required" 
                                ForeColor="Red">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator2" 
                                runat="server" 
                                ControlToValidate="PlanFixedCost" 
                                Display="Dynamic" 
                                ErrorMessage="PlanFixedCost expects an amount." 
                                ForeColor="Red" 
                                Operator="DataTypeCheck" 
                                Type="Currency">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr
                        valign="top">
                        <td
                            style="text-align: right;
                            background-color: #CC0000;
                            color: White;">
                            PlanVarCost:</td>
                        <td
                            style="width: 100px;">
                            <asp:TextBox ID="PlanVarCost" 
                                runat="server"></asp:TextBox>
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                runat="server" 
                                ControlToValidate="PlanVarCost" 
                                Display="Dynamic" 
                                ErrorMessage="PlanFixedCost is required" 
                                ForeColor="Red">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" 
                                runat="server" 
                                ControlToValidate="PlanVarCost" 
                                Display="Dynamic" 
                                ErrorMessage="PlanVarCost expects an amount." 
                                ForeColor="Red" 
                                Operator="DataTypeCheck" 
                                Type="Currency">*</asp:CompareValidator>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                class="td-bgcolor"
                align="left"
                nowrap="nowrap"
                style="width: 250px;">
                <asp:HyperLink
                    CssClass="Selected HideOnPreview PreviousPage"
                    ID="PreviousPageLink"
                    runat="server"
                    NavigateUrl="~/Default.aspx"
                    ToolTip="Click here to return to the previous page">Previous page</asp:HyperLink>
            </td>
            <td
                class="td-bgcolor"
                align="left"
                nowrap="nowrap"
                style="width: 250px;">
                <asp:Button
                    CssClass="Selected HideOnPreview Save"
                    ID="Edit"
                    runat="server"
                    Text="Add"
                    ToolTip="Add this record"
                    OnClick="Edit_Click" 
                    CausesValidation="false" 
                    OnClientClick="if(IsPageValid()){ ApplyProcessingStyle(this); } else { return false; }"
                    />
            </td>
            <td
                class="td-bgcolor"
                align="left"
                nowrap="nowrap"
                style="width: 250px;">
                <asp:Button
                    CssClass="Selected HideOnPreview Delete"
                    ID="ConfirmThenDelete"
                    runat="server"
                    CausesValidation="False"
                    Text="Delete"
                    Visible="False"
                    ToolTip="Delete this record"
                    OnClick="ConfirmThenDelete_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

