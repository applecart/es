
// Data Entry template
partial class frmDEtemplate : System.Web.UI.Page
{

#region "User defined methods" - Please define all your custom methods in this region

	private System.Data.DataRow Get1Record()
	{
        var sql =
        @"
        SELECT * FROM [Customers] WHERE CustID = @ID
        ";

		using (var rs = new Framework_Code.DataAccess("ES")) {
			rs.SetParameterValue("ID", GetRecId());
			return rs.ExecuteSql(sql).Rows[0];
		}

	}

    // Called from IsCancelRecordInsert(), IsCancelRecordUpdate()
    private bool HasFailedValidations()
    {
        bool retVal = false;

        // TBD

        return retVal;
    }

#endregion

#region "Template methods - Hooks"

	private void PrepFormControls()
	{
		// Called from Page_PreRender routine provides data for controls like DropDownList(s)
		// This is also a good place to direct focus to the first input control on Add and on Edit e.g: txtName.Focus()
        this.CustName.Focus();

	}

	private void PopulateFormAdd()
	{
		// Called from Page_PreRenderComplete and from Edit_Click, routine populates this fields on the form for an insert record operation

	}

	private void PopulateFormEdit()
	{
		// Called from Page_PreRenderComplete, routine populates the fields on this form for an update/delete record operation
		var row = Get1Record();
        this.CustName.Text = row["CustName"].ToString();
        this.CustEmail.Text = row["CustEmail"].ToString();
        this.PlanID.SelectedValueTry(row["PlanID"].ToString());  

	}

	private bool IsCancelRecordInsert()
	{
		// Called from from Edit_Click, routine inserts a record in the appropriate table
        // Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).

        if (HasFailedValidations()) return true; // Cancel Insert 


        var sql =
        @"

INSERT INTO [Customers]
(
     
	 [CustName]
	,[CustEmail]
	,[PlanID]

) VALUES (
     
	 @CustName
	,@CustEmail
	,@PlanID

)

";


        using (var rs = new Framework_Code.DataAccess("ES"))
        {

            rs.SetParameterValue("CustName", this.CustName.Text);
            rs.SetParameterValue("CustEmail", this.CustEmail.Text);
            rs.SetParameterValue("PlanID", this.PlanID.Text);

            rs.ExecuteNonQuery(sql);
        }

		return false;

	}

	private bool IsCancelRecordUpdate()
	{
		// Called from from Edit_Click, routine updates a record in the appropriate table
		// Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).

        if (HasFailedValidations()) return true; // Cancel Update 

		return false;

	}

	private bool IsCancelRecordDelete()
	{
		// Called from from ConfirmThenDelete_Click, routine flags a record in the appropriate table
		// Response.Redirect(PreviousPageLink.NavigateUrl) is automatically executed after editing a record on return value = False (Default).

		return false;

	}

	private bool IsModeInsert()
	{
		return Request.QueryString.Count == 0;

	}

	private int GetRecId()
	{
		return System.Convert.ToInt32(Request.QueryString[0]);

	}

#endregion
    /*
      --------------------------------------------------------------------------
      NOTE: ALTERING ANY METHOD BELOW THIS LINE WILL RESULT IN NON-CONFORMANCE
      --------------------------------------------------------------------------
    */
#region "Template methods - Standard"

	protected void Page_PreRender(object sender, System.EventArgs e)
	{
		PrepFormControls();

	}

	protected void Page_PreRenderComplete(object sender, System.EventArgs e)
	{
		if (!IsPostBack) {
			PreviousPageLink.NavigateUrl = Request.UrlReferrer.ToString();
			if (IsModeInsert()) {
				// Insert request
				PopulateFormAdd();
			} else {
				// Edit request for Update or Delete
				ConfirmThenDelete.Visible = true;
				Edit.Text = "Update";
				Edit.ToolTip = "Update this record";
				lblRecID.Text = GetRecId().ToString();
				PopulateFormEdit();
			}
		}

	}

	protected void Edit_Click(object sender, System.EventArgs e)
	{
        if (IsModeInsert())
        {
            if (!IsCancelRecordInsert()) Response.Redirect(PreviousPageLink.NavigateUrl);
        }
        else
        {
            if (!IsCancelRecordUpdate()) Response.Redirect(PreviousPageLink.NavigateUrl);
        }

        // Problem - form stays

    }

	protected void ConfirmThenDelete_Click(object sender, System.EventArgs e)
	{
		if (!IsCancelRecordDelete()) Response.Redirect(PreviousPageLink.NavigateUrl);

        // Problem - form stays

	}

#endregion

}

