
<%@ Page Language="C#" MasterPageFile="~/zmSite01.master" CodeFile="ListCustomersAndPlans.aspx.cs" Inherits="frmLISTtemplate"  Title="List Data" %>

<%@ Register Src="~/UserControls/Filter.ascx" TagName="Filter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table
        cellpadding="0"
        cellspacing="0"
        align="center">
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                class="td-bgcolor"
                align="left"
                valign="bottom"
                nowrap="nowrap">
            </td>
            <td
                class="td-bgcolor"
                align="left"
                valign="bottom"
                nowrap="nowrap"
                style="text-align: right;">
                <uc1:Filter
                    ID="Filter1"
                    runat="server" />
            </td>
        </tr>
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                class="td-bgcolor"
                align="left"
                colspan="2"
                nowrap="nowrap">
                <asp:GridView
                    
                    
                    ID="grdView"
                    
                    
                    runat="server"
                    
                    
                    AllowPaging="True"
                    
                    
                    AllowSorting="True"
                    
                    
                    CellPadding="3"
                    
                    
                    EmptyDataText="No data to list"
                    
                    
                    EnableViewState="False"
                    
                    
                    GridLines="Vertical"
                    
                    
                    ToolTip="Click on header to sort by that column"
                    
                    
                    BackColor="White"
                    
                    
                    BorderColor="#999999"
                    
                    
                    BorderStyle="None"
                    
                    
                    BorderWidth="1px" 
                    
                    AutoGenerateColumns="False" 
                    
                    DataSourceID="CustomerPlanData">
                    <FooterStyle
                        BackColor="#CCCCCC"
                        ForeColor="Black" />
                    <RowStyle
                        BackColor="#EEEEEE"
                        ForeColor="Black" />
                    <Columns>
                        <asp:BoundField
                            DataField="CustID"
                            HeaderText="CustID"
                            SortExpression="CustID">
                            <HeaderStyle
                                CssClass="gvColAlignRight" />
                            <ItemStyle
                                CssClass="gvColAlignRight" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CustName" 
                            HeaderText="CustName" 
                            SortExpression="CustName" />
                        <asp:BoundField DataField="CustEmail" 
                            HeaderText="CustEmail" 
                            SortExpression="CustEmail" />
                        <asp:BoundField 
                            DataField="PlanName" 
                            
                            HeaderText="PlanName" 
                            
                            SortExpression="PlanName" />
                        <asp:BoundField DataField="PlanFixedCost" 
                            DataFormatString="{0:c}" 
                            HeaderText="PlanFixedCost" 
                            SortExpression="PlanFixedCost" >
                            <HeaderStyle
                                CssClass="gvColAlignRight" />
                            <ItemStyle
                                CssClass="gvColAlignRight" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PlanVarCost" 
                            DataFormatString="{0:c}" 
                            HeaderText="PlanVarCost" 
                            SortExpression="PlanVarCost" >
                                                        <HeaderStyle
                                CssClass="gvColAlignRight" />
                            <ItemStyle
                                CssClass="gvColAlignRight" />
                        </asp:BoundField>
                    </Columns>
                    <SelectedRowStyle
                        BackColor="#008A8C"
                        Font-Bold="True"
                        ForeColor="White" />
                    <PagerStyle
                        CssClass="gvGridPager"
                        BackColor="#999999"
                        ForeColor="Black"
                        HorizontalAlign="Center" />
                    <HeaderStyle
                        CssClass="gvGridHeader"
                        ForeColor="White" />
                    <AlternatingRowStyle
                        BackColor="#DCDCDC" />
                    <SortedAscendingCellStyle
                        BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle
                        BackColor="#0000A9" />
                    <SortedDescendingCellStyle
                        BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle
                        BackColor="#000065" />
                </asp:GridView>
                <asp:SqlDataSource 
                    ID="CustomerPlanData" 
                    
                    runat="server" 
                    
                    ConnectionString="<%$ ConnectionStrings:ES %>" SelectCommand="
SELECT
  a.CustID
, a.CustName
, a.CustEmail
, b.PlanName
, b.PlanFixedCost
, b.PlanVarCost
FROM
  [Customers] AS a
INNER JOIN
   [Plans] AS b
ON
   a.PlanID = b.PlanID
WHERE
(
 a.CustName LIKE @LikeFilter
OR
a.CustEmail  LIKE @LikeFilter
OR
b.PlanName LIKE @LikeFilter
OR
CAST(a.CustID AS VARCHAR(11)) = @ExactFilter
)
ORDER BY
   1
   ">
                    <SelectParameters>
                        <asp:SessionParameter 
                            Name="LikeFilter" 
                            
                            SessionField="LikeFilter" />
                        <asp:SessionParameter 
                            Name="ExactFilter" 
                            
                            SessionField="ExactFilter" 
                            DefaultValue="0" />
                    </SelectParameters>
                </asp:SqlDataSource>
<%--ASP.NET Gotcha
The value of a query parameter is set to null when the ui control it is bound to, is empty.
However, the datasource cancels the SELECT query if any of the query parameter is null.
Override this behavior by specifying CancelSelectOnNullParameter="false" on the datasource definition.
--%>
            </td>
        </tr>
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                class="td-bgcolor"
                align="left"
                nowrap="nowrap">
                <asp:HyperLink
                    CssClass="Selected HideOnPreview PreviousPage"
                    ID="PreviousPageLink"
                    runat="server"
                    NavigateUrl="Default.aspx"
                    ToolTip="Click here to return to the previous page">Previous page</asp:HyperLink>
            </td>
            <td
                class="td-bgcolor"
                align="left"
                nowrap="nowrap"
                style="text-align: right;">
                <asp:HyperLink
                    
                    
                    CssClass="Selected HideOnPreview Add"
                    
                    
                    ID="AddNewRecords1"
                    
                    
                    runat="server"
                    
                    
                    NavigateUrl="CustomerEdit.aspx"
                    
                    
                    ToolTip="Click here for data entry form">Add Customer</asp:HyperLink>
            &nbsp;
                <asp:HyperLink
                    
                    
                    CssClass="Selected HideOnPreview Add"
                    
                    
                    ID="AddNewRecords0"
                    
                    
                    runat="server"
                    
                    
                    NavigateUrl="PlanEdit.aspx"
                    
                    
                    ToolTip="Click here for data entry form">Add Plan</asp:HyperLink>
                <asp:HyperLink
                    
                    CssClass="Selected HideOnPreview Add"
                    
                    ID="AddNewRecords"
                    
                    runat="server"
                    
                    NavigateUrl="ItemEdit.aspx"
                    
                    ToolTip="Click here for data entry form" 
                    Visible="False">Add new record(s)</asp:HyperLink>
            </td>

        </tr>

    </table>
</asp:Content>

