<%@ Page Language="C#" validateRequest="false" MasterPageFile="~/zmSite01.master" CodeFile="CustomerEdit.aspx.cs" Inherits="frmDEtemplate" title="Data Entry" %>


<asp:Content
    ID="Content1"
    ContentPlaceHolderID="ContentPlaceHolder1"
    runat="Server">
    <table
        cellpadding="0"
        cellspacing="0"
        align="center">
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                align="left"
                colspan="3"
                nowrap="nowrap">
                <asp:ValidationSummary
                    ID="valSummary"
                    runat="server"
                    ForeColor="Red" />
            </td>
        </tr>
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                class="td-bgcolor"
                align="left"
                colspan="3">
                <table
                    align="center">
                    <tr
                        valign="top">
                        <td
                            style="text-align: right;
                            background-color: #CC0000;
                            color: White;">
                            &nbsp;
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:Label
                                ID="lblRecID"
                                runat="server"
                                Text="New Record"></asp:Label>
                        </td>
                        <td
                            style="width: 100px;">
                        </td>
                    </tr>
                    <tr
                        valign="top">
                        <td
                            style="text-align: right;
                            background-color: #CC0000;
                            color: White;">
                            CustName:
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:TextBox ID="CustName" 
                                runat="server"></asp:TextBox>
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                runat="server" 
                                ControlToValidate="CustName" 
                                Display="Dynamic" 
                                ErrorMessage="CustName is required" 
                                ForeColor="Red">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr
                        valign="top">
                        <td
                            style="text-align: right;
                            background-color: #CC0000;
                            color: White;">
                            CustEmail:</td>
                        <td
                            style="width: 100px;">
                            <asp:TextBox ID="CustEmail" 
                                runat="server"></asp:TextBox>
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:RequiredFieldValidator 
                                ID="RequiredFieldValidator2" 
                                
                                runat="server" 
                                
                                ControlToValidate="CustEmail" 
                                
                                Display="Dynamic" 
                                
                                ErrorMessage="CustEmail is required" 
                                
                                ForeColor="Red">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                                runat="server" 
                                ControlToValidate="CustEmail" 
                                Display="Dynamic" 
                                ErrorMessage="CustEmail is not valid" 
                                ForeColor="Red" 
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr
                        valign="top">
                        <td
                            style="text-align: right;
                            background-color: #CC0000;
                            color: White;">
                            CustPlan:</td>
                        <td
                            style="width: 100px;">
                            <asp:DropDownList 
                                ID="PlanID" 
                                
                                runat="server" 
                                AppendDataBoundItems="True" 
                                DataSourceID="PlanData" 
                                DataTextField="PlanName" 
                                DataValueField="PlanID">
                                <asp:ListItem Value=" ">Please select a plan</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="PlanData" 
                                runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ES %>" 
                                ProviderName="<%$ ConnectionStrings:ES.ProviderName %>" 
                                SelectCommand="SELECT [PlanName], [PlanID] FROM [Plans] ORDER BY 1">
                            </asp:SqlDataSource>
                        </td>
                        <td
                            style="width: 100px;">
                            <asp:RequiredFieldValidator 
                                ID="RequiredFieldValidator3" 
                                
                                runat="server" 
                                
                                ControlToValidate="PlanID" 
                                
                                Display="Dynamic" 
                                
                                ErrorMessage="CustPlan is required" 
                                
                                ForeColor="Red">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr
            valign="top">
            <td
                class="td-filler">
            </td>
            <td
                class="td-bgcolor"
                align="left"
                nowrap="nowrap"
                style="width: 250px;">
                <asp:HyperLink
                    CssClass="Selected HideOnPreview PreviousPage"
                    ID="PreviousPageLink"
                    runat="server"
                    NavigateUrl="~/Default.aspx"
                    ToolTip="Click here to return to the previous page">Previous page</asp:HyperLink>
            </td>
            <td
                class="td-bgcolor"
                align="left"
                nowrap="nowrap"
                style="width: 250px;">
                <asp:Button
                    CssClass="Selected HideOnPreview Save"
                    ID="Edit"
                    runat="server"
                    Text="Add"
                    ToolTip="Add this record"
                    OnClick="Edit_Click" 
                    CausesValidation="false" 
                    OnClientClick="if(IsPageValid()){ ApplyProcessingStyle(this); } else { return false; }"
                    />
            </td>
            <td
                class="td-bgcolor"
                align="left"
                nowrap="nowrap"
                style="width: 250px;">
                <asp:Button
                    CssClass="Selected HideOnPreview Delete"
                    ID="ConfirmThenDelete"
                    runat="server"
                    CausesValidation="False"
                    Text="Delete"
                    Visible="False"
                    ToolTip="Delete this record"
                    OnClick="ConfirmThenDelete_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

