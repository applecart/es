﻿<%@ Page Title="" Language="C#" MasterPageFile="~/zmSite01.master" AutoEventWireup="true" CodeFile="RunAs.aspx.cs" Inherits="App_Users_AliasMe_RunAs" %>

<%@ Register src="~/UserControls/enumber.ascx" tagname="enumber" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Run as:
    <uc1:enumber ID="UserID" runat="server" />
    <br />
    <asp:Button ID="SubmitConfirm" runat="server" Text="Submit" 
        UseSubmitBehavior="False" OnClick="SubmitConfirm_Click" />
    <asp:Button ID="ResetAliasing" runat="server" Text="Reset Aliasing" 
        UseSubmitBehavior="False" CausesValidation="False" OnClick="ResetAliasing_Click" />
    <br />
    For testing only: this feature will not be available on the production build.
</asp:Content>

