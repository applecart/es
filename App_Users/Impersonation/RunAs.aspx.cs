﻿

public partial class App_Users_AliasMe_RunAs : System.Web.UI.Page
{

    protected void Page_Init(object sender, System.EventArgs e)
    {
        Response.Redirect("~/NotificationPages/unauthorized.aspx");
        if (!Permissions.IsInRole(Permissions.UserNameReal(), "Administrators"))
        {
            // if the user gets here then user does not have the necessary role: redirect him to unauthorized page
            Response.Redirect("~/NotificationPages/unauthorized.aspx");
        }

    }

    protected void SubmitConfirm_Click(object sender, System.EventArgs e)
    {
        SetAliasing(this.UserID.Text);

    }

    protected void ResetAliasing_Click(object sender, System.EventArgs e)
    {
        this.UserID.Text = string.Empty;
        SetAliasing(string.Empty);

    }

    protected void Page_PreRender(object sender, System.EventArgs e)
    {
        if (!IsPostBack)
        {
            var who = new Framework_Code.Who(CommonCode.GetVconfig("ApplicationName"));
            this.UserID.Text = who.UserId;
        }
        Response.Cache.SetNoStore(); // do not cache this page
        this.UserID.Focus(); 

    }

    private void SetAliasing(string aliasId)
    {
        var who = new Framework_Code.Who(CommonCode.GetVconfig("ApplicationName"));
        who.UserId = aliasId;

    }

}