﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        var ex = HttpContext.Current.Server.GetLastError().GetBaseException();
        if (ex.GetType() == typeof(HttpException))
        {
            HttpException httpException = (HttpException) ex;
            if (httpException.GetHttpCode() == 404) return;
        }

        var oEmail = new Framework_Code.Email();
        oEmail.eMail_From("donotreply@coca-cola.com", "Please do not reply");
        oEmail.eMail_Subject("Error notification from " + CommonCode.GetVconfig("ApplicationName"));
        oEmail.eMail_Body(string.Format("{0}<p>{1}", GetEnvelopeInfo(), Server.GetLastError().ToString().Replace(Environment.NewLine , "<br />")));
        foreach (string email in CommonCode.GetVconfig("EmailErrorNotificationTo").Split(','))
        {
            oEmail.eMail_To(email);
        }
        oEmail.eMail_Send();

    }

    // Called from Application_Error
    private string GetEnvelopeInfo()
    {
        using (var adHocData = new Framework_Code.AdhocData())
        {
            adHocData.SetColumnValue("Web Server", Environment.MachineName);
            adHocData.SetColumnValue("User Id", Permissions.UserName());
            adHocData.SetColumnValue("User Name", Permissions.EmployeeName());
            adHocData.AddRow();
            return adHocData.ToHtmlTable();
        }

    }

  
    
    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
