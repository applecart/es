﻿<%@ Page Title="" Language="C#" MasterPageFile="~/zmSite01.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <B>Oops, it seems like you have bookmarked a page which no longer exists on this 
        web site!</B></p>
    <p>
        We revamped this web site to accomodate evolving business requirements more 
        efficiently.<br />
        Please email the administrator of this application to learn how to access the feature you 
        are trying.</p>
</asp:Content>

