﻿/*-----------------------------------------------------------------------------------------------------------------------

An abstract class to free your web site from InProc Session dependancy.
    The InProc Session variables can get "lost" while operating 
        in a web farm scenario (web server > 1) or
        in a web garden scenario (worker process > 1) 

Preparing for custom session state management
    Drop this class in folder App_Code\AbstractClasses
    Define a public class SessionStateModeCustom : AbstractClasses.CustomSessionStateBase
    Implement the following string methods to specify the name of the session state table and the name of the connection string to perform CRUD ops on this table.
        GetSessionStateTableName(): // specify the name of the table to deal session state
        GetConnectionStringName():  // specify the name of the connection string for CRUD ops on the table specified to deal session state

Next specify your custom session state provider in the web.config/system.web  
        <sessionState mode="Custom" customProvider="InTable">
          <providers>
            <add name="InTable" type="SessionStateModeCustom"/>
          </providers>
        </sessionState>

That is it. Never lose those pesky session variables to InProc insanity again.

Script to re-create the session state table 
    CREATE TABLE [Sessions] (
	    [Key] VARCHAR(80) NOT NULL, 
	    [ValueBase64] VARCHAR(MAX), 
	    [ExpireDtUtc] DATETIME NOT NULL,
	    CONSTRAINT [PK_Sessions] PRIMARY KEY ([Key])
    )

Denny Jacob - 04/15/2015
Last Updated On - 05/25/2017

-----------------------------------------------------------------------------------------------------------------------*/

namespace AbstractClasses
{

    public abstract class CustomSessionStateBase : System.Web.SessionState.SessionStateStoreProviderBase
    {

        public override System.Web.SessionState.SessionStateStoreData CreateNewStoreData(System.Web.HttpContext context, int timeout)
        {
            return new System.Web.SessionState.SessionStateStoreData(new System.Web.SessionState.SessionStateItemCollection(), System.Web.SessionState.SessionStateUtility.GetSessionStaticObjects(context), timeout);

        }

        public override void CreateUninitializedItem(System.Web.HttpContext context, string id, int timeout)
        {
            Add(id, null);

        }

        public override void Dispose()
        {

        }

        public override void EndRequest(System.Web.HttpContext context)
        {

        }

        public override System.Web.SessionState.SessionStateStoreData GetItem(System.Web.HttpContext context, string id, out bool locked, out System.TimeSpan lockAge, out object lockId, out System.Web.SessionState.SessionStateActions actions)
        {
            lockAge = System.TimeSpan.Zero;
            lockId = null;
            locked = false;
            actions = System.Web.SessionState.SessionStateActions.None;
            return Retrieve(id, context, 0);

        }

        public override System.Web.SessionState.SessionStateStoreData GetItemExclusive(System.Web.HttpContext context, string id, out bool locked, out System.TimeSpan lockAge, out object lockId, out System.Web.SessionState.SessionStateActions actions)
        {
            return GetItem(context, id, out locked, out lockAge, out lockId, out actions);

        }

        public override void InitializeRequest(System.Web.HttpContext context)
        {

        }

        public override void ReleaseItemExclusive(System.Web.HttpContext context, string id, object lockId)
        {

        }

        public override void RemoveItem(System.Web.HttpContext context, string id, object lockId, System.Web.SessionState.SessionStateStoreData item)
        {
            Remove(id);

        }

        public override void ResetItemTimeout(System.Web.HttpContext context, string id)
        {

        }

        public override void SetAndReleaseItemExclusive(System.Web.HttpContext context, string id, System.Web.SessionState.SessionStateStoreData item, object lockId, bool newItem)
        {
            if (newItem)
            {
                Add(id, item);
            }
            else
            {
                Update(id, item);
            }

        }

        public override bool SetItemExpireCallback(System.Web.SessionState.SessionStateItemExpireCallback expireCallback)
        {
            return false;

        }

        #region "Non-Public methods"

        protected abstract string GetConnectionStringName();

        protected abstract string GetSessionStateTableName();

        private System.Web.SessionState.SessionStateStoreData Retrieve(string key, System.Web.HttpContext context, int timeout)
        {
            DelayExpireDtUtc(key);
            using (var rs = new Framework_Code.DataAccess(GetConnectionStringName()))
            {
                rs.SetParameterValue("Key", key);
                return Deserialize(context, (string) rs.ExecuteScalar(System.String.Format("SELECT ValueBase64 FROM {0} WHERE [Key] = @Key", GetSessionStateTableName())), timeout);
            }

        }

        private void Add(string key, System.Web.SessionState.SessionStateStoreData value)
        {
            using (var rs = new Framework_Code.DataAccess(GetConnectionStringName()))
            {
                rs.SetParameterValue("Key", key);
                rs.SetParameterValue("ValueBase64", Serialize(value));
                rs.SetParameterValue("ExpireDtUtc", System.DateTime.UtcNow.AddMinutes(SlideExpirationMinutes).ToString("yyyy-MM-dd HH:mm:ss"));
                rs.ExecuteNonQuery(System.String.Format("INSERT INTO {0} ([Key], ValueBase64, ExpireDtUtc) VALUES (@Key, @ValueBase64, @ExpireDtUtc)",GetSessionStateTableName()));
            }
            PurgeExpiredSessionVariables();

        }

        private void Remove(string key)
        {
            using (var rs = new Framework_Code.DataAccess(GetConnectionStringName()))
            {
                rs.SetParameterValue("Key", key);
                rs.ExecuteNonQuery(System.String.Format("DELETE FROM {0} WHERE [Key] = @Key", GetSessionStateTableName()));
            }

        }

        private void Update(string key, System.Web.SessionState.SessionStateStoreData value)
        {
            using (var rs = new Framework_Code.DataAccess(GetConnectionStringName()))
            {
                rs.SetParameterValue("ValueBase64", Serialize(value));
                rs.SetParameterValue("ExpireDtUtc", System.DateTime.UtcNow.AddMinutes(SlideExpirationMinutes).ToString("yyyy-MM-dd HH:mm:ss"));
                rs.SetParameterValue("Key", key);
                if (rs.ExecuteNonQuery(System.String.Format("UPDATE {0} SET ValueBase64 = @ValueBase64, ExpireDtUtc = @ExpireDtUtc WHERE [Key] = @Key", GetSessionStateTableName())) == 0) { Add(key, value); }
            }

        }


        private int SlideExpirationMinutes = 29;

        // Called from Add
        private void PurgeExpiredSessionVariables()
        {
            using (var rs = new Framework_Code.DataAccess(GetConnectionStringName()))
            {
                rs.SetParameterValue("Now", System.DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"));
                rs.ExecuteNonQuery(System.String.Format("DELETE FROM {0} WHERE ExpireDtUtc < @Now", GetSessionStateTableName()));
            }

        }

        // Called from Retrieve
        private void DelayExpireDtUtc(string key)
        {
            using (var rs = new Framework_Code.DataAccess(GetConnectionStringName()))
            {
                rs.SetParameterValue("ExpireDtUtc", System.DateTime.UtcNow.AddMinutes(SlideExpirationMinutes).ToString("yyyy-MM-dd HH:mm:ss"));
                rs.SetParameterValue("Key", key);
                rs.ExecuteNonQuery(System.String.Format("UPDATE {0} SET ExpireDtUtc = @ExpireDtUtc WHERE [Key] = @Key", GetSessionStateTableName()));
            }

        }

        // Called from Add, Update
        private string Serialize(System.Web.SessionState.SessionStateStoreData values)
        {
            using (var ms = new System.IO.MemoryStream())
            {
                using (var writer = new System.IO.BinaryWriter(ms))
                {
                    var value = (System.Web.SessionState.SessionStateItemCollection)values.Items;

                    if (value != null) value.Serialize(writer);

                    return System.Convert.ToBase64String(ms.ToArray());

                }
            }

        }

        // Called from Retrieve
        private System.Web.SessionState.SessionStateStoreData Deserialize(System.Web.HttpContext context, string serializedItems, int timeout)
        {
            if (serializedItems == null) serializedItems = string.Empty;

            using (var ms = new System.IO.MemoryStream(System.Convert.FromBase64String(serializedItems)))
            {
                var sessionItems = new System.Web.SessionState.SessionStateItemCollection();

                if (ms.Length > 0)
                {
                    using (var reader = new System.IO.BinaryReader(ms))
                    {
                        sessionItems = System.Web.SessionState.SessionStateItemCollection.Deserialize(reader);
                    }
                }

                return new System.Web.SessionState.SessionStateStoreData(sessionItems, System.Web.SessionState.SessionStateUtility.GetSessionStaticObjects(context), timeout);

            }

        }

        #endregion

    } // class

} // namespace
