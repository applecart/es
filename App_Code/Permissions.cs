﻿/*

A class to help resolve a user attributes and authorization roles by querying the active directory of KO domain and the roles table of the respective apps

Public methods:
---------------
    UserNameReal: Returns actual unaliased username as reported by Request.LogonUserIdentity.Name 
        but without the domain information.
    UserName: Returns "active" user id which may be from a cookie or appSettings value for RunAs key
 
    The following methods has an overload to query by a given user id
        IsInRole: Returns whether or not the current user is in the role being queried.
        EmployeeName: Returns the value in DisplayName column of the current/queried user
        EmailAddress: Returns the value in EmailAddress column of the current/queried user
        CompanyCode: Returns the company code of the current/queried user
 
*/

public static class Permissions
{

    public static string UserNameReal()
    {
        // removed domain information from Request.LogonUserIdentity.Name
        return System.Text.RegularExpressions.Regex.Replace(System.Web.HttpContext.Current.Request.LogonUserIdentity.Name      
            , ".*\\\\(.*)", "$1", System.Text.RegularExpressions.RegexOptions.None);

    }

    public static string UserName()
    {
        string retVal = CommonCode.GetVconfig("RunAs");
        // alias as user defined in app settings key RunAs

        // Special addition to this app is users ability to set aliasing aka RunAs as cookie
        // This feature will not kick in if there is a RunAs defined i.e. RunAs is not empty.
        if(string.IsNullOrWhiteSpace(retVal)) {
            var who = new Framework_Code.Who(CommonCode.GetVconfig("ApplicationName"));
            retVal = who.UserId; 
        }
        // Special addition block ends

        if (string.IsNullOrWhiteSpace(retVal)) retVal = UserNameReal();

        return retVal.ToUpperInvariant().Trim();

    }

    public static bool IsInRole(string role)
    {
        return IsInRole(UserName(), role.Trim());

    }

    public static bool IsInRole(string userId, string role)
    {
        return true;

    }

    public static string EmployeeName()
    {
        return EmployeeName(UserName());

    }

    public static string EmployeeName(string userId)
    {
        return GetUserAttribute(userId, UserAttribute.DisplayName);

    }

    public static string EmailAddress()
    {
        return EmailAddress(UserName());

    }

    public static string EmailAddress(string userId)
    {
        return GetUserAttribute(userId, UserAttribute.EmailAddress);

    }

    public static string CompanyCode()
    {
        return CompanyCode(UserName());

    }

    public static string CompanyCode(string userId)
    {
        return GetUserAttribute(userId, UserAttribute.CompanyCode);

    }

    private enum UserAttribute
    {
         DisplayName
        ,EmailAddress
        ,CompanyCode

    }

    // Called from EmployeeName(), EmailAddress(), CompanyCode()
    private static string GetUserAttribute(string userId, UserAttribute resolve)
    {
        string retVal = "Anonymous";

        switch (resolve)
        {
            case UserAttribute.DisplayName:
                // TODO
                break;
            case UserAttribute.EmailAddress:
                // TODO
                break;
            case UserAttribute.CompanyCode:
                // TODO
                break;
            default:
                throw new System.NotImplementedException(); 
        }

        return retVal; 
    }

}

