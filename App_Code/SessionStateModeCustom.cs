﻿

public class SessionStateModeCustom : AbstractClasses.CustomSessionStateBase
{

    protected override string GetConnectionStringName()
    {
        return "InTableSessions";
    }

    protected override string GetSessionStateTableName()
    {
        return "Sessions";
    }

}