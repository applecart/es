﻿/*

 Implementation for IIS 7.0
    <system.webServer> 
        <modules>
          <!--Registers the http module for IIS 7.0 running in integrated mode-->
          <add name="SiteMinderModule" type="SiteMinderModule"/>
        </modules>
    </system.webServer>

Implementation for IIS 6.0
    <httpModules>
      <!--Registers the http module for IIS 6.0 and IIS 7.0 running in classic mode-->
      <add name="SiteMinderModule" type="SiteMinderModule"/>
    </httpModules>


*/

public class SiteMinderModule : System.Web.IHttpModule, System.Web.SessionState.IRequiresSessionState
{

    /// <summary>
    /// Register for events that are handled within this module
    /// </summary>
    /// <param name="context">Application object</param>
    public void Init(System.Web.HttpApplication context)
    {
        context.PreRequestHandlerExecute += new System.EventHandler(Application_PreRequestHandler);

    }

    /// <summary>
    /// This event occurs just before ASP.NET begins executing a handler such a aspx page.
    /// We use this event to extract the SiteMinder headers from the request and construct
    /// our principal object
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Application_PreRequestHandler(System.Object sender, System.EventArgs e)
	{
		// Retrieve all SiteMinder variables from the request header
		var siteMinder = System.Web.HttpContext.Current.Request.Headers;
		if (siteMinder["HTTP_OBLIX_UID"] != null) 
        {
			// Create GenericPrincipal with authentication type "SiteMinder".
			System.Security.Principal.GenericIdentity webIdentity = new System.Security.Principal.GenericIdentity(siteMinder["HTTP_OBLIX_UID"], "SiteMinder");
			System.Security.Principal.GenericPrincipal principal = new System.Security.Principal.GenericPrincipal(webIdentity, new string[2] {string.Empty, string.Empty});
            System.Web.HttpContext.Current.User = principal;
			System.Threading.Thread.CurrentPrincipal = principal;
		}

	}

    /// <summary>
    /// Required Dispose Method
    /// </summary>
    public void Dispose()
    {

    }

}
