﻿

public static class CommonCode
{

    public static string GetVconfig(string key)
    {
        return System.Configuration.ConfigurationManager.AppSettings[key];

    }

    public static bool IsProduction()
    {
        bool retVal = true;
        return retVal; // this should be resolved from a scalar ufn

    }

    public static void DownloadSaveAs(string fileName, byte[] bytes)
    {
        var oResponse = System.Web.HttpContext.Current.Response;
        var quotationMark = (char)34;
        oResponse.Clear();
        oResponse.ContentType = "application/octet-stream";
        oResponse.AddHeader("Content-Disposition", string.Format("attachment; filename={1}{0}{1}", fileName, quotationMark));
        oResponse.BinaryWrite(bytes);
        oResponse.Flush();
        oResponse.SuppressContent = true;
        System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();

    }

}
